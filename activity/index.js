const students = []

function addStudent(name){
	students.push(name);
	console.log(name + ' was added to the student list.')
}

addStudent("Carlo");
addStudent("Adrian");
addStudent("Aaron");

function countStudent() {
	console.log("The number of students is " + students.length);
}

countStudent()

function printStudent() {
	//first sort the array in alphanumeric
	//logs each student name in the browser(using .forEach())
	students.sort()
	students.forEach(function(name) {
		console.log(name);	
})
}

printStudent()

function findStudent(name) {
	
	const lowercase = students.map(function(n){
		return n.toLowerCase()
	})

	let indexStudent = lowercase.indexOf(name)

	if (name == lowercase[indexStudent]){
	console.log(name, "is an enrollee")
	}
	else {
		console.log(name, "is not an enrollee")
	}
}

findStudent("Carlo");
findStudent("Boyet");