//storing multiple values using variables

let student1 = "2020-01-17";
let student2 = "2020-01-20";
let student3 = "2020-01-25";
let student4 = "2020-02-25";

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);


//storing multiple values in an array

const studentNumbers = ["2020-01-17", "2020-01-20", "2020-01-25", "2020-02-25"];
console.log(studentNumbers);

//outputs the value of the element in the index 1 of the studentNumbers array
//all arrays starts with index 0
//syntax: arrayName[index];
console.log(studentNumbers[1]);
console.log('Index 3 of studentNumber: ', studentNumbers[3]);
console.log(studentNumbers[4]);//returns undefined since there is no value defined for the fourth index of the studentNumbers


//arrays are declared using the square brackets AKA Array Literals

const emptyArray = []; //declares an empty array
const grades = [75, 85.5, 92, 94];//an array of numbers
const computerBrands = ["Acer", "Asus", "Lenovo", "Apple", "Redfox", "Gateway"]

console.log("Empty Array Sample", emptyArray);
console.log(grades);
console.log("Computer Brands Array: ", computerBrands);


//all elements inside an array should have the same data tpe
//all elements insisde an array should be related with each other

const mixedArr = [12, "Asus", undefined, null, {}];

const fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log(fruits);

//.push() function adds an element at the end of an array

fruits.push('Mango');
console.log(fruits);//fruits + mango

//fruits = "Prutas"
	//will return an error since we cannot assign the const variable fruits to another value.

//.pop() - removes the last element in the array
fruits.pop();
console.log(fruits);//fruits - mango

//.unshift() - adds one or more elements at the beginning of the array

fruits.unshift('strawberry');//adds strawberry at the beginning of the fruits array
console.log(fruits);//strawberry + fruits

fruits.unshift('banana', 'raisins');
console.log(fruits);//banana + raisins + fruits

//.shift() - removes the first element in our array

let removedFruit = fruits.shift()//expected output is banana is removed from the fruits array
console.log('You successfully removed the fruit: ', removedFruit);//removed banana

//.reverse() - reverses the order of elements in an array
fruits.reverse()
console.log(fruits);//reversed order


const tasks = [
	'drink html', 
	'eat JS', 
	'inhale CSS',
	'bake sass'
]
console.log(tasks);

//.sort arranges the elements in alphanumeric sequence
tasks.sort()
console.log(tasks);//sorted alphabetically

const oddNumbers = [1, 3, 9, 5, 7]
console.log(oddNumbers);

oddNumbers.sort()
console.log(oddNumbers);//1,3,5,7,9 - sorted alphanumeric


const countries = ['US', 'PH', 'CAN', 'SG', 'PH'];

//.indexOf() - finds the index of a given element where it is FIRST found

let indexCountry = countries.indexOf('PH');
console.log(indexCountry);//1

//.lastIndexOf() - finds the index of a given element where it is LAST found
let lastIndexCountry = countries.lastIndexOf('PH');
console.log(lastIndexCountry);//4

//.toString - converts an array into a single value that is separated by a comma
console.log(countries);
console.log(countries.toString());


//.concat() - joins 2 or more arrays
const subTasksA = ['drink html', 'eat Js']
const subTasksB = ['inhale css', 'breathe sass']
const subTasksC = ['get git', 'be node']

const subTasks = subTasksA.concat(subTasksB, subTasksC)
console.log(subTasks);

//.join() - converts an array into a single value and is separated by a specified character
console.log(subTasks.join('-'))//-
console.log(subTasks.join(' @'))// @
console.log(subTasks.join(' x'))// x



//syntax:
/* 
	arrayName.forEach(function(eachElement)){
		console.log(eachElement)
	}
*/

//.forEach() - iterates each element inside an array
//we pass each element as the parameter of the function declared inside the .forEach()

//we have users array
//we want to iterate the users array using the .forEach() function
//each element is stored inside the variable called user
//logged in the browser the value of each user / each element inside the users array

const users = ['blue', 'alexis', 'bianca', 'nikko', 'adrian']
console.log(users[0]);//blue
console.log(users[1]);//alexis

users.forEach(function(user) {
	//console.log(user);
	if(user === 'blue'){
		console.log(user);
	}
})



const numberList = [1,2,3,4,5,6,7,8,9]

numberList.forEach(function(n){
	if(n % 2 === 0){
		console.log(n, ' even');
	}
	else {
		console.log(n, ' odd');
	}
})

//length property returns the number of elements inside an array
console.log(numberList.length);//9


//.map() - iterate each element and returns a new array depending on the result of the function
//.map() is useful when we will manipulate/change elements inside our array
//.map() allows us to not touch/manipulate the original array
const numbersData = numberList.map(function(n){
	return n * n
})

console.log(numbersData);
console.log(numberList);//not manipulated



//multidimensional arrays

const chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
]

console.log(chessBoard);//logs the whole chessBoard array
console.log(chessBoard[0]);// a1,b1,c1,d1,e1,f1,g1,h1
console.log(chessBoard[0][1])// b1
console.log(chessBoard[2][5])// f3




